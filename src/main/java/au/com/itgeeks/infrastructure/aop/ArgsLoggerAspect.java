package au.com.itgeeks.infrastructure.aop;

import java.util.concurrent.TimeUnit;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ArgsLoggerAspect {

  @Autowired
  private ApplicationEventPublisher eventPublisher;

  @Around("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
  public Object logExecutionTime(final ProceedingJoinPoint joinPoint) throws Throwable {

    final long start = System.currentTimeMillis();

    final Object proceed = joinPoint.proceed();

    final long executionTime = System.currentTimeMillis() - start;

    // ApplicationEvent arg0 = null ;
    // eventPublisher.publishEvent(arg0);

    System.out.println(joinPoint.getSignature() + " executed in " + executionTime + "ms ");

    return proceed;
  }

  @Around("execution(public * **..controller..**.*(..))")
  public Object measureMethodExecutionTime(final ProceedingJoinPoint pjp) throws Throwable {
    final long start = System.nanoTime();
    final Object retval = pjp.proceed();
    final long end = System.nanoTime();
    final String methodName = pjp.getSignature().getName();
    System.out.println("Execution of " + methodName + " took "
        + TimeUnit.NANOSECONDS.toMillis(end - start) + " ms");
    return retval;
  }


  @Around("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
  public Object printArguments(final ProceedingJoinPoint pjp) throws Throwable {

    Object[] objs = pjp.getArgs();
    
    String str = new String();
    for (final Object obj : objs) {
      str = str + (obj != null ? obj.toString() : "") + " ";
    }
    
    Object proceed = pjp.proceed(objs);

    System.out.println(pjp.getSignature() + " executed with args { " + str + " } return { " + proceed.toString() + " }");
    
    return proceed;
  }


}
