package au.com.itgeeks.infrastructure.config;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.interceptor.PerformanceMonitorInterceptor;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import au.com.itgeeks.infrastructure.interceptor.LoggerInterceptor;

@Configuration
@EnableAspectJAutoProxy
@Aspect
public class ApplicationConfig {
  
  @Bean
  public PerformanceMonitorInterceptor performanceMonitorInterceptor() {
      return new PerformanceMonitorInterceptor(true);
  }

  @Bean
  public Advisor performanceMonitorAdvisor() {
      AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
      pointcut.setExpression("au.com.itgeeks.infrastructure.interceptor.LoggerInterceptor.monitor()");
      return new DefaultPointcutAdvisor(pointcut, performanceMonitorInterceptor());
  }
  
  @Bean
  public LoggerInterceptor loggerInterceptor() {
      return new LoggerInterceptor();
  }
  
}
