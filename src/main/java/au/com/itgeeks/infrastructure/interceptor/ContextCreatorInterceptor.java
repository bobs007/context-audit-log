package au.com.itgeeks.infrastructure.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import au.com.itgeeks.infrastructure.model.ContextHolder;


/**
 * 
 * @author bobs
 *
 */
public class ContextCreatorInterceptor extends HandlerInterceptorAdapter {

  private static final Logger logger = LoggerFactory.getLogger(ContextCreatorInterceptor.class);
  

  @Override
  public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response,
      final Object handler) throws Exception {

    logger.debug("");
    
    ContextHolder.get();
    
    logger.debug("");
    
    return false;
  }

}
