package au.com.itgeeks.infrastructure.audit;

import java.util.Collection;

import org.springframework.stereotype.Service;

import au.com.itgeeks.infrastructure.model.Audit;

@Service
public interface AuditService {
  
  void addAuditEvent(Collection<Audit> auditEvents);

  Collection<Audit> listAllAuditEvent();
  
  Collection<Audit> listAuditEventsByUser(String userId);
  
  Collection<Audit> listAuditEventsByEvent(String event);
  
}
