package au.com.itgeeks.infrastructure.model;

import java.util.List;
import java.util.Queue;
import java.util.UUID;

public class ContextHolder {

  private final static InheritableThreadLocal<Context> context =
      new InheritableThreadLocal<Context>();

  public static class Context {

    private Queue<Audit> listAuditEvents;
    private final AuthenticatedContext singInContext;
    private List<AuthenticatedContext> adoptedContext;
    private final UUID uuid;

    public Context(final UUID uuid, final AuthenticatedContext singInContext) {
      this.uuid = uuid;
      this.singInContext = singInContext;
    }

    public Context addAdoptedContext(final AuthenticatedContext authenticatedContext) {
      adoptedContext.add(authenticatedContext);
      return this;
    }

    public Context addAuditEvent(final Audit audit) {
      listAuditEvents.add(audit);
      return this;
    }

    public Queue<Audit> getListAuditEvents() {
      return listAuditEvents;
    }

    public AuthenticatedContext getSingInContext() {
      return singInContext;
    }

    public List<AuthenticatedContext> getAdoptedContext() {
      return adoptedContext;
    }

    public UUID getUuid() {
      return uuid;
    }

  }

  private ContextHolder() {}

  public static Context init(final UUID uuid, final AuthenticatedContext singInContext) {
    final Context con = new Context(uuid, singInContext);
    context.set(con);
    return context.get();
  }

  public static Context get() throws UnsupportedOperationException {
    if (context.get() != null) {
      throw new UnsupportedOperationException("Not initialised.");
    }
    return context.get();
  }

}
