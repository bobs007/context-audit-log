package au.com.itgeeks.infrastructure.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import au.com.itgeeks.infrastructure.aop.LogMethodArgsResponse;

@RestController
public class HomeController {
  
  @RequestMapping(value = "/username", method = RequestMethod.GET)
  @ResponseBody
  @LogMethodArgsResponse
  public String home(String arg) {
    return "test";
  }
  
}
